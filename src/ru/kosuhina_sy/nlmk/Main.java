package ru.kosuhina_sy.nlmk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        Factorial factorial = new Factorial();
        InvocationHandler handler = new InvocationHandlerImpl(factorial);
        IFactorial proxy = (IFactorial) Proxy.newProxyInstance(factorial.getClass().getClassLoader(),
                Factorial.class.getInterfaces(), handler);

        System.out.println(proxy.getFactorial(7));
        System.out.println(proxy.getFactorial(8));
        System.out.println(proxy.getFactorial(9));
        System.out.println(proxy.getFactorial(7));
        System.out.println(proxy.getFactorial(7));

    }
}
