package ru.kosuhina_sy.nlmk;

public class Factorial implements IFactorial{
    public int getFactorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }
}
