package ru.kosuhina_sy.nlmk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class InvocationHandlerImpl implements InvocationHandler {
    private Factorial factorial;

    public InvocationHandlerImpl(Factorial factorial) {
        this.factorial = factorial;
    }

    Map<Integer, Integer> cache = new HashMap<>(10);

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (cache.containsKey(args[0])) {
            System.out.println("Значение из кэша");
            return cache.get(args[0]);
        }
        else {
            Integer result = (Integer) method.invoke(factorial, args);
            cache.put((Integer) args[0], result);
            return result;
        }
    }

}
