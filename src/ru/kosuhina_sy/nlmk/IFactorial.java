package ru.kosuhina_sy.nlmk;

public interface IFactorial {
        int getFactorial(int n);
}
